package View;

import Controller.Controller;
import Exceptions.UnknownOptionException;
import Model.Expressions.ArithmeticExpression;
import Model.Expressions.ValueExpression;
import Model.Expressions.VariableExpression;
import Model.ProgramState.ProgramState;
import Model.Statements.*;
import Model.Types.BoolType;
import Model.Types.IntType;
import Model.Values.BoolValue;
import Model.Values.IntValue;
import Repository.MemoryRepository;

import java.util.Scanner;

public class App {
    public void start() {
        try {
            // int v ; v=2 ; print(v)
            var programExample1 = new ProgramState(new CompoundStatement(
                    new VariableDeclarationStatement(new IntType(), "v"),
                    new AssignmentStatement("v", new ValueExpression(new IntValue(2))),
                    new PrintStatement(new VariableExpression("v"))
            ));

            // int a ; int b ; a=2+3*5 ; b=a+1 ; print(b)
            var programExample2 = new ProgramState(new CompoundStatement(
                    new VariableDeclarationStatement(new IntType(), "a"),
                    new VariableDeclarationStatement(new IntType(), "b"),
                    new AssignmentStatement("a", new ArithmeticExpression(
                            new ValueExpression(new IntValue(2)),
                            '+',
                            new ArithmeticExpression(
                                    new ValueExpression(new IntValue(3)),
                                    '*',
                                    new ValueExpression(new IntValue(5))
                            )
                    )),
                    new AssignmentStatement("b", new ArithmeticExpression(
                            new VariableExpression("a"),
                            '+',
                            new ValueExpression(new IntValue(1))
                    )
                    ),
                    new PrintStatement(new VariableExpression("b"))
            ));

            // bool a ; int v ; a=true ; ( if a then (v=2) else (v=3) ) ; print(v)
            var programExample3 = new ProgramState(new CompoundStatement(
                    new VariableDeclarationStatement(new BoolType(), "a"),
                    new VariableDeclarationStatement(new IntType(), "v"),
                    new AssignmentStatement("a", new ValueExpression(new BoolValue(true))),
                    new IfStatement(
                            new VariableExpression("a"),
                            new AssignmentStatement("v", new ValueExpression(new IntValue(2))),
                            new AssignmentStatement("v", new ValueExpression(new IntValue(3)))
                    ),
                    new PrintStatement(new VariableExpression("v"))
            ));

            // bool a ; a = 1
            var programExample4 = new ProgramState(new CompoundStatement(
                    new VariableDeclarationStatement(new BoolType(), "a"),
                    new AssignmentStatement("a", new ValueExpression(new IntValue(1)))
            ));

            boolean running = true;
            while (running) {
                Scanner in = new Scanner(System.in);
                ProgramState chosenProgramState = null;

                displayPrograms();

                System.out.print("Choose a program to execute: ");
                String option = in.next();

                System.out.print("Choose a log file path: ");
                String logFilePath = in.next();

                switch (option) {
                    case "0" -> running = false;
                    case "1" -> chosenProgramState = programExample1;
                    case "2" -> chosenProgramState = programExample2;
                    case "3" -> chosenProgramState = programExample3;
                    case "4" -> chosenProgramState = programExample4;
                    default -> throw new UnknownOptionException("Choose a value from 1 to 3.");
                }


                if (running) {
                    var repository = new MemoryRepository();
                    var controller = new Controller(repository);

                    controller.setDisplayFlag(true);
                    controller.setLogFilePath(logFilePath);
                    controller.addProgramState(chosenProgramState);
                    controller.fullExecution();
                }
            }
        }
        catch (Exception e) {
            System.out.println(e.getClass().getCanonicalName() + ": " + e.getMessage());
        }
    }

    public void displayPrograms() {
        System.out.println("1. int v ; v=2 ; print(v)");
        System.out.println("2. int a ; int b ; a=2+3*5 ; b=a+1 ; print(b)");
        System.out.println("3. bool a ; int v ; a=true ; ( if a then (v=2) else (v=3) ) ; print(v)");
        System.out.println("4. bool a ; a = 1");
    }
}
