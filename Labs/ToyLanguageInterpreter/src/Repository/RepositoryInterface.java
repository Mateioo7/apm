package Repository;

import Exceptions.EmptyStackException;
import Model.ProgramState.ProgramState;

import java.io.IOException;
import java.util.ArrayList;

public interface RepositoryInterface {
    void logProgramStateExecution() throws IOException, EmptyStackException;
    void addProgramState(ProgramState programState);
    ArrayList<ProgramState> getProgramStates();
    ProgramState getCurrentProgramState();

    void setLogFilePath(String logFilePath);
}
