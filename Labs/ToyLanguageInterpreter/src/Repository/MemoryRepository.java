package Repository;

import Exceptions.EmptyStackException;
import Model.ADTs.StackInterface;
import Model.ProgramState.ProgramState;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Stack;

public class MemoryRepository implements RepositoryInterface {
    private final ArrayList<ProgramState> programStates = new ArrayList<>();
    private String logFilePath;

    @Override
    public void logProgramStateExecution() throws IOException, EmptyStackException {
        // empty file
        //PrintWriter pw = new PrintWriter(logFilePath);
        //pw.close();

        var fw = new FileWriter(logFilePath, true);
        var bw = new BufferedWriter(fw);
        var out = new PrintWriter(bw);

        var programState = getCurrentProgramState();
        var executionStack = programState.getExecutionStack();
        var symbolsTable = programState.getSymbolsTable();
        var outputList = programState.getOutputList();
        var fileTable = programState.getFileTable();


        out.println("Execution stack:");
        out.println(executionStack.toString());
        out.println("Symbols table:");
        out.println(symbolsTable.toString());
        out.println("Output list:");
        out.println(outputList.toString());
        out.println("File table:");
        out.println(fileTable.toString());
        out.println("\n\n");

        out.close();
    }

    @Override
    public void addProgramState(ProgramState programState) {
        programStates.add(programState);
    }

    @Override
    public ArrayList<ProgramState> getProgramStates() {
        return programStates;
    }

    @Override
    public ProgramState getCurrentProgramState() {
        return programStates.get(0);
    }

    @Override
    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }
}
