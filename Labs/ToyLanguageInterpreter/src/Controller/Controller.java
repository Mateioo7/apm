package Controller;

import Exceptions.DivisionByZeroException;
import Exceptions.EmptyStackException;
import Exceptions.UndefinedVariableException;
import Model.ProgramState.ProgramState;
import Repository.RepositoryInterface;
import Exceptions.InvalidTypeException;

import java.io.IOException;

public class Controller {
    private final RepositoryInterface repository;
    private boolean displayFlag;

    public Controller(RepositoryInterface repository) {
        this.repository = repository;
    }

    public void setLogFilePath(String logFilePath) {
        repository.setLogFilePath(logFilePath);
    }

    public void setDisplayFlag(boolean value) {
        displayFlag = value;
    }

    public void addProgramState(ProgramState programState) {
        repository.addProgramState(programState);
    }

    public ProgramState oneStepExecution(ProgramState programState) throws EmptyStackException,
            DivisionByZeroException, UndefinedVariableException, InvalidTypeException {
        var executionStack = programState.getExecutionStack();

        if (executionStack.isEmpty()) {
            throw new EmptyStackException("Execution stack is empty.");
        }

        var currentStatement = executionStack.pop();
        return currentStatement.execute(programState);
    }

    public void fullExecution() throws InvalidTypeException, EmptyStackException,
            UndefinedVariableException, DivisionByZeroException, IOException {

        var programState = repository.getCurrentProgramState();
        if (displayFlag) {
            displayProgramState();
        }
        repository.logProgramStateExecution();
        while (!programState.getExecutionStack().isEmpty()) {
            oneStepExecution(programState);
            repository.logProgramStateExecution();
            if (displayFlag) {
                displayProgramState();
            }
        }
    }

    public void displayProgramState() {
        var programState = repository.getCurrentProgramState();
        System.out.println(programState.toString());
    }
}
