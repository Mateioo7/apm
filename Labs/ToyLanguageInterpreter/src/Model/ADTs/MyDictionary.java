package Model.ADTs;

import Exceptions.UndefinedVariableException;

import java.util.HashMap;
import java.util.Map;

public class MyDictionary<K,V> implements DictionaryInterface<K,V> {
    private final HashMap<K,V> dictionary;

    public MyDictionary() {
        dictionary = new HashMap<K, V>();
    }

    @Override
    public V insert(K key, V value) {
        return dictionary.put(key, value);
    }

    @Override
    public V remove(K key, V value) throws UndefinedVariableException {
        if (!dictionary.containsKey(key))
            throw new UndefinedVariableException("Can't remove variable " + key + "since is not defined.");

        return dictionary.remove(key);
    }

    @Override
    public V lookup(K key) throws UndefinedVariableException {
        if (!dictionary.containsKey(key))
            throw new UndefinedVariableException("Variable " + key + " not defined.");

        return dictionary.get(key);
    }

    @Override
    public boolean isDefined(K key) {
        return dictionary.containsKey(key);
    }

    @Override
    public void update(K key, V newValue) {
        dictionary.put(key, newValue);
    }

    @Override
    public boolean isEmpty() {
        return dictionary.isEmpty();
    }

    @Override
    public String toString() {
        var sb = new StringBuilder();
        for (Map.Entry<K, V> e : dictionary.entrySet()) {
            sb.append(e.getKey()).append(" --> ").append(e.getValue()).append("\n");
        }
        if (!sb.isEmpty()) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }
}
