package Model.ADTs;

import Exceptions.EmptyStackException;
import java.util.Stack;

public class MyStack<T> implements StackInterface<T> {
    private final Stack<T> stack;

    public MyStack() {
        stack = new Stack<T>();
    }

    @Override
    public T peek() {
        return stack.lastElement();
        //return stack.peek();
    }

    @Override
    public T pop() throws EmptyStackException {
        if (stack.isEmpty())
            throw new EmptyStackException("Stack is empty.");

        return stack.pop();
    }

    @Override
    public void push(T e) {
        stack.push(e);
    }

    @Override
    public boolean isEmpty() {
        return stack.empty();
    }

    @Override
    public String toString() {
        var sb = new StringBuilder();
        for (T e : stack) {
            sb.append(e.toString()).append("\n");
        }
        if (!sb.isEmpty()) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }
}
