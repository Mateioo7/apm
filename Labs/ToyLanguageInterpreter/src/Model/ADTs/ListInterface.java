package Model.ADTs;

public interface ListInterface<T> {
    void insert(T e);
    void remove(T e);
    boolean isEmpty();
    String toString();
}
