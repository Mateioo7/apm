package Model.ADTs;

import Exceptions.EmptyStackException;

public interface StackInterface<T> {
    T peek();
    T pop() throws EmptyStackException;
    void push(T e);
    boolean isEmpty();
    String toString();
}
