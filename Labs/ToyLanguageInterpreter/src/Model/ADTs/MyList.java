package Model.ADTs;

import java.util.ArrayList;

public class MyList<T> implements ListInterface<T> {
    private final ArrayList<T> list;

    public MyList() {
        list = new ArrayList<T>();
    }

    @Override
    public void insert(T e) {
        list.add(e);
    }

    @Override
    public void remove(T e) { list.remove(e); }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public String toString() {
        var sb = new StringBuilder();
        for (T e : list) {
            sb.append(e).append("\n");
        }
        if (!sb.isEmpty()) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }
}
