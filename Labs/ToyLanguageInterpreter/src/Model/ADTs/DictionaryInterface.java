package Model.ADTs;

import Exceptions.UndefinedVariableException;

public interface DictionaryInterface<K,V> {
    V insert(K key, V value);
    V remove(K key, V value) throws UndefinedVariableException;
    V lookup(K key) throws UndefinedVariableException;
    boolean isDefined(K key);
    void update(K key, V newValue);
    boolean isEmpty();
    String toString();
}
