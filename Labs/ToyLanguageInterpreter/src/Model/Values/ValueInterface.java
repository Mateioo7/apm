package Model.Values;

import Model.Types.TypeInterface;

public interface ValueInterface {
    boolean equals(Object object);
    TypeInterface getType();
    String toString();
}
