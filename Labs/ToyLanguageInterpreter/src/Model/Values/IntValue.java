package Model.Values;

import Model.Types.IntType;
import Model.Types.TypeInterface;

public class IntValue implements ValueInterface {
    private final int value;

    public IntValue(int val) {
        value = val;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof IntValue))
            return false;
        return value == ((IntValue) obj).getValue();
    }

    public int getValue() {
        return value;
    }

    @Override
    public TypeInterface getType() {
        return new IntType();
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
