package Model.Values;

import Model.Types.BoolType;
import Model.Types.TypeInterface;

public class BoolValue implements ValueInterface {
    private final boolean value;

    public BoolValue(boolean val) {
        value = val;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BoolValue))
            return false;
        return value == ((BoolValue) obj).getValue();
    }

    public boolean getValue() {
        return value;
    }

    @Override
    public TypeInterface getType() {
        return new BoolType();
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
