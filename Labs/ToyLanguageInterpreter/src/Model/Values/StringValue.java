package Model.Values;

import Model.Types.StringType;
import Model.Types.TypeInterface;

public class StringValue implements ValueInterface {
    private final String value;

    public StringValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof StringValue))
            return false;
        return value.equals(((StringValue) obj).getValue());
    }

    public String getValue() {
        return value;
    }

    @Override
    public TypeInterface getType() {
        return new StringType();
    }

    @Override
    public String toString() {
        return value;
    }
}
