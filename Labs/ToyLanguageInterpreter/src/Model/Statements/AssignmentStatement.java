package Model.Statements;

import Exceptions.DivisionByZeroException;
import Exceptions.InvalidTypeException;
import Exceptions.UndefinedVariableException;
import Model.Expressions.ExpressionInterface;
import Model.ProgramState.ProgramState;
import Model.Types.TypeInterface;
import Model.Values.ValueInterface;

public class AssignmentStatement implements StatementInterface {
    private final String variableName;
    private final ExpressionInterface expression;

    public AssignmentStatement(String variableName, ExpressionInterface expression) {
        this.variableName = variableName;
        this.expression = expression;
    }

    @Override
    public ProgramState execute(ProgramState programState) throws DivisionByZeroException,
            UndefinedVariableException, InvalidTypeException {
        var symbolsTable = programState.getSymbolsTable();

        if (symbolsTable.isDefined(variableName)) {
            ValueInterface expressionValue = expression.evaluate(symbolsTable);
            TypeInterface variableType = (symbolsTable.lookup(variableName)).getType();

            if (expressionValue.getType().equals(variableType))
                symbolsTable.update(variableName, expressionValue);
            else throw new InvalidTypeException("Declared variable " + variableName + " and type of the assigned " +
                    "expression do not match.");
        }
        else throw new UndefinedVariableException("Variable " + variableName + " was not declared.");

        return programState;
    }

    @Override
    public String toString() {
        return variableName + " = " + expression.toString();
    }
}
