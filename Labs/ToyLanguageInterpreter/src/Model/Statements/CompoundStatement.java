package Model.Statements;

import Exceptions.StatementException;
import Model.ADTs.StackInterface;
import Model.ProgramState.ProgramState;

import java.util.Arrays;

public class CompoundStatement implements StatementInterface {
    private final StatementInterface firstStatement;
    private final StatementInterface secondStatement;

    public CompoundStatement(StatementInterface firstStatement, StatementInterface secondStatement) {
        this.firstStatement = firstStatement;
        this.secondStatement = secondStatement;
    }

    public CompoundStatement(StatementInterface... statementInterfaces) throws StatementException {
        if (statementInterfaces.length < 2) {
            throw new StatementException("Compound statement needs at least 2 statements.");
        }

        firstStatement = statementInterfaces[0];
        if (statementInterfaces.length == 2) {
            secondStatement = statementInterfaces[1];
        }
        else {
            secondStatement = new CompoundStatement(Arrays.stream(statementInterfaces).skip(1).toArray(StatementInterface[]::new));
        }
    }

    @Override
    public ProgramState execute(ProgramState programState) {
        StackInterface<StatementInterface> executionStack = programState.getExecutionStack();
        executionStack.push(secondStatement);
        executionStack.push(firstStatement);
        return programState;
    }

    @Override
    public String toString() {
        return "( " + firstStatement.toString() + " ; " + secondStatement.toString() + " )";
    }
}
