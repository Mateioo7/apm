package Model.Statements;

import Model.ProgramState.ProgramState;
import Model.Types.BoolType;
import Model.Types.IntType;
import Model.Types.TypeInterface;

public class VariableDeclarationStatement implements StatementInterface {
    private final TypeInterface variableType;
    private final String variableName;

    public VariableDeclarationStatement(TypeInterface variableType, String variableName) {
        this.variableType = variableType;
        this.variableName = variableName;
    }

    @Override
    public ProgramState execute(ProgramState programState) {
        var symbolsTable = programState.getSymbolsTable();
        //var executionStack = programState.getExecutionStack();
        //executionStack.pop();

        if (!symbolsTable.isDefined(variableName)) {
            if (variableType.equals(new IntType())) {
                symbolsTable.insert(variableName, variableType.defaultValue());
            }
            else if (variableType.equals(new BoolType())) {
                symbolsTable.insert(variableName, variableType.defaultValue());
            }
        }

        return programState;
    }

    @Override
    public String toString() {
        return variableType.toString() + " " + variableName;
    }
}
