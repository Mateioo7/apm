package Model.Statements;

import Exceptions.DivisionByZeroException;
import Exceptions.InvalidTypeException;
import Exceptions.UndefinedVariableException;
import Model.Expressions.ExpressionInterface;
import Model.ProgramState.ProgramState;
import Model.Values.BoolValue;
import Model.Types.BoolType;

public class IfStatement implements StatementInterface {
    private final ExpressionInterface condition;
    private final StatementInterface thenStatement;
    private final StatementInterface elseStatement;

    public IfStatement(ExpressionInterface expressionCondition, StatementInterface thenStatement,
                       StatementInterface elseStatement) {
        this.condition = expressionCondition;
        this.thenStatement = thenStatement;
        this.elseStatement = elseStatement;
    }

    @Override
    public ProgramState execute(ProgramState programState) throws DivisionByZeroException,
            UndefinedVariableException, InvalidTypeException {
        var symbolTable = programState.getSymbolsTable();
        var conditionType = condition.evaluate(symbolTable);

        if (conditionType.getType().equals(new BoolType())) {
            BoolValue conditionValue = (BoolValue)conditionType;
            var executionStack = programState.getExecutionStack();

            if (conditionValue.getValue()) {
                executionStack.push(thenStatement);
            }
            else {
                executionStack.push(elseStatement);
            }
        }
        else throw new InvalidTypeException("Conditional expression is not boolean.");

        return programState;
    }

    @Override
    public String toString() {
        return "( if (" + condition.toString() + ") then (" + thenStatement.toString() + ") else ("
                + elseStatement.toString() + ") )";
    }
}
