package Model.Statements;

import Exceptions.DivisionByZeroException;
import Exceptions.InvalidTypeException;
import Exceptions.UndefinedVariableException;
import Model.Expressions.ExpressionInterface;
import Model.ProgramState.ProgramState;

public class PrintStatement implements StatementInterface {
    private final ExpressionInterface value;

    public PrintStatement(ExpressionInterface value) {
        this.value = value;
    }

    @Override
    public ProgramState execute(ProgramState programState) throws DivisionByZeroException,
            UndefinedVariableException, InvalidTypeException {
        var symbolsTable = programState.getSymbolsTable();
        var output = programState.getOutputList();
        output.insert(value.evaluate(symbolsTable));
        return programState;
    }

    @Override
    public String toString() {
        return "print(" + value.toString() + ")";
    }
}
