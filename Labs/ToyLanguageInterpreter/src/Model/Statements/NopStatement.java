package Model.Statements;

import Model.ProgramState.ProgramState;

public class NopStatement implements StatementInterface {
    @Override
    public ProgramState execute(ProgramState programState) {
        return programState;
    }

    @Override
    public String toString() {
        return "null";
    }
}
