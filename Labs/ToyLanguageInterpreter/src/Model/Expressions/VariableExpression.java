package Model.Expressions;

import Exceptions.UndefinedVariableException;
import Model.ADTs.MyDictionary;
import Model.Values.ValueInterface;

public class VariableExpression implements ExpressionInterface {
    private final String variableName;

    public VariableExpression(String variableName) {
        this.variableName = variableName;
    }

    @Override
    public ValueInterface evaluate(MyDictionary<String, ValueInterface> symbolsTable)
            throws UndefinedVariableException {
        return symbolsTable.lookup(variableName);
    }

    @Override
    public String toString() {
        return variableName;
    }
}
