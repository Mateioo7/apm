package Model.Expressions;

import Exceptions.DivisionByZeroException;
import Exceptions.InvalidTypeException;
import Exceptions.UndefinedVariableException;
import Model.ADTs.MyDictionary;
import Model.Values.ValueInterface;

public interface ExpressionInterface {
    ValueInterface evaluate(MyDictionary<String, ValueInterface> symbolsTable)
            throws DivisionByZeroException, InvalidTypeException, UndefinedVariableException;

    String toString();
}
