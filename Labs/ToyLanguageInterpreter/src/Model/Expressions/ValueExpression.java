package Model.Expressions;

import Model.ADTs.MyDictionary;
import Model.Values.ValueInterface;

public class ValueExpression implements ExpressionInterface {
    private final ValueInterface value;

    public ValueExpression(ValueInterface value) {
        this.value = value;
    }

    @Override
    public ValueInterface evaluate(MyDictionary<String, ValueInterface> symbolsTable) {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
