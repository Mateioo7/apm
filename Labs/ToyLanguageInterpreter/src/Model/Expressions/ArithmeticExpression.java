package Model.Expressions;

import Exceptions.DivisionByZeroException;
import Exceptions.InvalidTypeException;
import Exceptions.UndefinedVariableException;
import Model.ADTs.MyDictionary;
import Model.Types.IntType;
import Model.Values.IntValue;
import Model.Values.ValueInterface;

public class ArithmeticExpression implements ExpressionInterface {
    private final ExpressionInterface expression1, expression2;
    private final char operation;

    public ArithmeticExpression(ExpressionInterface expression1, char operation, ExpressionInterface expression2) {
        this.expression1 = expression1;
        this.expression2 = expression2;
        this.operation = operation;
    }

    @Override
    public ValueInterface evaluate(MyDictionary<String, ValueInterface> symbolsTable)
            throws DivisionByZeroException, InvalidTypeException, UndefinedVariableException {

        ValueInterface value1, value2;
        value1 = expression1.evaluate(symbolsTable);

        if (value1.getType().equals(new IntType())) {
            value2 = expression2.evaluate(symbolsTable);

            if (value2.getType().equals(new IntType())) {
                IntValue integer1 = (IntValue)value1;
                IntValue integer2 = (IntValue)value2;

                int integerValue1 = integer1.getValue();
                int integerValue2 = integer2.getValue();

                switch (operation) {
                    case '+': return new IntValue(integerValue1 + integerValue2);
                    case '-': return new IntValue(integerValue1 - integerValue2);
                    case '*': return new IntValue(integerValue1 * integerValue2);
                    case '/':
                        if (integerValue2 == 0) throw new DivisionByZeroException("Division by zero.");
                        else return new IntValue(integerValue1 / integerValue2);
                }
            }
            else throw new InvalidTypeException("Second operand is not an integer.");
        }
        else throw new InvalidTypeException("First operand is not an integer.");

        // to make ide shut up
        return value1;
    }

    @Override
    public String toString() {
        return expression1.toString() + operation + expression2.toString();
    }
}
