package Model.Expressions;

import Exceptions.DivisionByZeroException;
import Exceptions.InvalidTypeException;
import Exceptions.UndefinedVariableException;
import Model.ADTs.MyDictionary;
import Model.Types.BoolType;
import Model.Values.BoolValue;
import Model.Values.ValueInterface;

public class LogicalExpression implements ExpressionInterface {
    private final ExpressionInterface expression1, expression2;
    private final String operation;

    public LogicalExpression(ExpressionInterface expression1, String operation, ExpressionInterface expression2) {
        this.expression1 = expression1;
        this.operation = operation;
        this.expression2 = expression2;
    }

    @Override
    public ValueInterface evaluate(MyDictionary<String, ValueInterface> symbolsTable)
            throws DivisionByZeroException, InvalidTypeException, UndefinedVariableException {
        ValueInterface value1, value2;
        value1 = expression1.evaluate(symbolsTable);

        if (value1.getType().equals(new BoolType())) {
            value2 = expression2.evaluate(symbolsTable);

            if (value2.getType().equals(new BoolType())) {
                BoolValue bool1 = (BoolValue)value1;
                BoolValue bool2 = (BoolValue)value2;

                boolean boolValue1 = bool1.getValue();
                boolean boolValue2 = bool2.getValue();

                switch (operation) {
                    case "and": return new BoolValue(boolValue1 && boolValue2);
                    case "or": return new BoolValue(boolValue1 || boolValue2);
                }
            }
            else throw new InvalidTypeException("Second operand is not boolean.");
        }
        else throw new InvalidTypeException("First operand is not boolean.");

        // to make ide shut up
        return value1;
    }

    @Override
    public String toString() {
        return expression1.toString() + operation + expression2.toString();
    }
}
