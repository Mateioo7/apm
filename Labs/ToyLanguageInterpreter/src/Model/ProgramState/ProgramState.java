package Model.ProgramState;

import Model.ADTs.*;
import Model.Statements.StatementInterface;
import Model.Values.ValueInterface;

import java.io.BufferedReader;

public class ProgramState {
    private MyStack<StatementInterface> executionStack = new MyStack<>();
    private MyDictionary<String, ValueInterface> symbolsTable = new MyDictionary<>();
    private MyDictionary<String, BufferedReader> fileTable = new MyDictionary<>();
    private MyList<ValueInterface> outputList = new MyList<>();
    //private MyList<valueInterface> outputList = new MyList<>();
    //private final StatementInterface originalProgram;

    public ProgramState(StatementInterface program) {
        executionStack.push(program);
    }

    public MyStack<StatementInterface> getExecutionStack() {
        return executionStack;
    }

    public void setExecutionStack(MyStack<StatementInterface> exeStack) {
        executionStack = exeStack;
    }

    public MyDictionary<String, ValueInterface> getSymbolsTable() {
        return symbolsTable;
    }

    public void setSymbolsTable(MyDictionary<String, ValueInterface> symTable) {
        symbolsTable = symTable;
    }

    public MyList<ValueInterface> getOutputList() {
        return outputList;
    }

    public void setOutputList(MyList<ValueInterface> out) {
        outputList = out;
    }

    @Override
    public String toString() {
        return "Execution stack:\n" + executionStack.toString() + "\nSymbols table:\n" +
                symbolsTable.toString() + "\nOutput list:\n" + outputList.toString() + "\n";
    }

    public DictionaryInterface<String, BufferedReader> getFileTable() {
        return fileTable;
    }

    public void setFileTable(MyDictionary<String, BufferedReader> fileTable) {
        this.fileTable = fileTable;
    }
}
