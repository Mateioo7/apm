package Model.Types;

import Model.Values.ValueInterface;

public interface TypeInterface {
    boolean equals(Object object);
    ValueInterface defaultValue();
    String toString();
}
